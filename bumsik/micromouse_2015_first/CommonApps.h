#ifndef CommonApps_h
#define CommonApps_h

#include "Arduino.h"

class CommonApps{
public:
	static int filter_int(int min, int value, int max);
	static bool is_in_range_int(int min, int value, int max);
};
#endif