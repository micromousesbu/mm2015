#include "Arduino.h"
#include "fsm_table.h"

const transition * ps_transition_ptr[4] = {
	in_center_transitions,
	going_front_transitions,
	turning_left_transitons,
	turning_right_transitions
};

void fsm(state *ps, event even_value)
{
	// Search the array of transition structures corresponding to the
	// present state for the transition structure that has has event
	// field value that is equal to current input key value or equal 
	// eol
	int i;
	for (i = 0; (ps_transition_ptr[*ps][i].event_val != even_value)
		&& (ps_transition_ptr[*ps][i].event_val != eol); i++){
	};

	// i now has the value of the index of the transition structure
	// corresponding to the current event value.

	// Make present state equal to the next state value of the current
	// transition structure.
	*ps = ps_transition_ptr[*ps][i].next_state;

	// Invoke the task function pointer to by the task function pointer
	// of the current transition structure.
	ps_transition_ptr[*ps][i].task_ptr();
}