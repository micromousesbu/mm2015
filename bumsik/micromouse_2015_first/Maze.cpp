#include "Maze.h"
#include "Cell.h"
#include "Position.h"

Maze::Maze() {
	unsigned char x;
	unsigned char y;
	//Wrap the wall
	for (x = 1; x < MAX_X_POSITION; x++){
		cells[x][MIN_Y_POSITION] = new Cell(unknown, unknown, exist, unknown);
		cells[x][MAX_Y_POSITION] = new Cell(exist, unknown, unknown, unknown);
	}
	for (y = 1; y < MAX_Y_POSITION; y++){
		cells[MIN_X_POSITION][y] = new Cell(unknown, unknown, unknown, exist);
		cells[MAX_X_POSITION][y] = new Cell(unknown, exist, unknown, unknown);
	}
	cells[MIN_X_POSITION][MIN_Y_POSITION] = new Cell(unknown, unknown, exist, exist);
	cells[MAX_X_POSITION][MIN_Y_POSITION] = new Cell(unknown, exist, exist, unknown);
	cells[MIN_X_POSITION][MAX_Y_POSITION] = new Cell(exist, unknown, unknown, exist);
	cells[MAX_X_POSITION][MAX_Y_POSITION] = new Cell(exist, exist, unknown, unknown);

	//Fill the last
	for (x = MIN_X_POSITION + 1; x < MAX_X_POSITION; x++){
		for (y = MIN_Y_POSITION + 1; y < MAX_Y_POSITION; y++){
			cells[x][y] = new Cell();
		}
	}
}
Cell* Maze::getCell(Position position) {
	return cells[position.x][position.y];
}


Cell* Maze::getYPlusCell(Position position) {
	return cells[position.x][position.y+1];
}

Cell* Maze::getXPlusCell(Position position) {
	return cells[position.x + 1][position.y];
}

Cell* Maze::getYMinusCell(Position position) {
	return cells[position.x][position.y - 1];
}

Cell* Maze::getXMinusCell(Position position) {
	return cells[position.x -1][position.y];
}
