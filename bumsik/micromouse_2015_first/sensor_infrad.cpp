#include "Arduino.h"
#include "sensor_infrad.h"
#include "CommonApps.h"

ProximitySensor::ProximitySensor(){

}

ProximitySensor::ProximitySensor(unsigned char analogPin){
	this->analogPin = analogPin;
	pinMode(analogPin, INPUT);
}

int ProximitySensor::getProximity(){
	return this->proximity;
}

int ProximitySensor::measureProximity(){
	this->proximity = CommonApps::filter_int(ProximitySensor::MIN_PROXIMITY,
		analogRead(this->analogPin),ProximitySensor::MAX_PROXIMITY);
	return proximity;
}