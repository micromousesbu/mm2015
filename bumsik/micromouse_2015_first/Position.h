#ifndef Position_h
#define Position_h

#include "Arduino.h"


class Position {

public:
	unsigned char x;
	unsigned char y;

	Position();

	Position(unsigned char x, unsigned char y);
};
#endif