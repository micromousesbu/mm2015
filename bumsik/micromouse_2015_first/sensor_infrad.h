//=========================================================================
//			MUST READ - INFORMATION ABOUT THE ARDUINO TIMER
//   Timer0 : Timer0 is a 8bit timer. In the Arduino world timer0 is been 
// used for the timer functions, like delay(), millis() and micros().If you
// change timer0 registers, this may influence the Arduino timer function.
// So you should know what you are doing.
//   Timer1 : Timer1 is a 16bit timer.
// In the Arduino world the Servo library uses timer1 on 
// Arduino Uno(timer5 on Arduino Mega).
//   Timer2 : Timer2 is a 8bit timer like timer0. In the Arduino work the 
// tone() function uses timer2.
//=========================================================================
//			HOW TO CONTROL MOTOR USING PWM or timer interrupt
// For Micro pro (ATmega32U4), There is Timer/Counter 0(8bit)/1(16bit)
// /3(16bit) and 4(10bit). All three has CTC PWM MODE, which has various
// frequency. (f=fclk/(2N(1+OCRnA), N= 1, 8, 64, 256, or 1024)
// For Arduino Uno (ATmega328), There is Timer/Counter 0(8bit)/1(16bit)
// /2(8bit)
// They have to use OCnA pin and OCnA Interrupt and set OCR0A.
//
// FOr 8 bit -
// TCCRxA=0x42 (COM0A1:0(7:6)=01 - Toggle OC0A, =00 not Toggle)(WGM01:02(1:0)=10)
// TCCR0B=0x0X : WGM02(3)=0 and
// CS02:0 (2:0)= 1,2,3,4,5 = 1, 8, 64, 256, 1024 =N.
// if you want to off the Counter, CS02:CS00 =0.
// To turn on/off the intruppt at TIMSKx-> OCIExB:OCIExA:TOIEx(2:0) =x:1/0:x 
//
// for 16-bit -
// TCCRxA=0x40, (WGM01:02(1:0)=00)
// TCCRxB=0x8 (WGM13:12(4:3)=01), others are the same as above
// for normal mode, TCCRxA = 0, TCCRxB=0+CS12:10
//
// In arduino, name of the ISR is : ISR(TIMERx_COMPy_vect)
// 
// Timer/Counter Pin info -
// For Micro pro (ATmega32U4)-
// OC0A =PB7orPin12 in ATmega but not used for Micro pro!!!
// OC1A:B:C = PB5:PB6:PB7orPin9:10:X, 
// OC3A = PC6orPin5, OC4A:B:D = PC7:PB6:PD7orPinX:10:6
//
// For Arduino Uno (ATmega328)-
// OC0A:B = PD6:PD5orPin6:5, OC1A:B = PB1:2orPin9:10, OC2A:B =PB3:PD3or Pin11:3
//=========================================================================
#ifndef ProximitySensor_h
#define ProximitySensor_h

#include "Arduino.h"

class ProximitySensor{
private:
	unsigned char analogPin;	//Arduino Uno : Pin0~5
								//Pro Micro : Pin 4,6,8,9,10,18,19,20,21
	int proximity;
public:
	const static int MIN_PROXIMITY = 5;
	const static int MAX_PROXIMITY = 1000;
	const static int MIN_PROXIMITY_TO_WALL = 900;
	const static int PROPER_PROXIMITY_TO_WALL = 800;
	ProximitySensor();
	ProximitySensor(unsigned char analogPin);
	int getProximity();
	int measureProximity();
};
#endif