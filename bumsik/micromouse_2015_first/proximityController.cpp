#include "Arduino.h"
#include "sensor_infrad.h"
#include "proximityController.h"
#include "CommonApps.h"

ProximityController::ProximityController(ProximitySensor left, ProximitySensor center, ProximitySensor right){
	this->leftSensor = left;
	this->centerSensor = center;
	this->rightSensor = right;
}

void ProximityController::measureAll(){
	leftSensor.measureProximity();
	centerSensor.measureProximity();
	rightSensor.measureProximity();
}

char ProximityController::compareLeftAndRight(){
	int compare = leftSensor.getProximity() - rightSensor.getProximity();
	if (CommonApps::is_in_range_int(-50, compare, 50))
		return 0;
	else if (compare < 0)
		return -1;
	else
		return 1;
}

int ProximityController::getLeftProximity(){
	return this->leftSensor.getProximity();
}

int ProximityController::getFrontProximity(){
	return this->centerSensor.getProximity();
}

int ProximityController::getRightProximity(){
	return this->rightSensor.getProximity();
}