#include "Arduino.h"
#include "motorController.h"
#include "motor_stepper.h"

MotorController::MotorController(Motor left, Motor right){
	this->leftMotor = left;
	this->rightMotor = right;
}

void MotorController::decreaseSteps(unsigned char step){
	this->leftMotor.decreaseStep(step);
	this->rightMotor.decreaseStep(step);
}

void MotorController::setSpeedRatings(unsigned char speedRating){
	this->leftMotor.setSpeedRating(speedRating);
	this->rightMotor.setSpeedRating(speedRating);
}

void MotorController::moveForward(int step){

	TCCR1A = 0x40; 
	TCCR3A = 0x40;	//start toggleing the signal
	

	this->leftMotor.rotateForward(step);
	this->rightMotor.rotateForward(step);
}

void MotorController::moveForward(int step, unsigned char speedRating){

	TCCR1A = 0x40;
	TCCR3A = 0x40;	//start toggleing the signal


	setSpeedRatings(speedRating);
	moveForward(step);
}

void MotorController::moveBackward(int step){

	TCCR1A = 0x40;
	TCCR3A = 0x40;	//start toggleing the signal


	this->leftMotor.rotateBackward(step);
	this->rightMotor.rotateBackward(step);
}

void MotorController::moveBackward(int step, unsigned char speedRating){

	TCCR1A = 0x40;
	TCCR3A = 0x40;	//start toggleing the signal


	setSpeedRatings(speedRating);
	moveBackward(step);
}

void MotorController::rotateLeft(float angle, float radius, unsigned char speedRating){

}

void MotorController::rotateRight(float angle, float radius, unsigned char speedRating){

}

void MotorController::pause(){

}

void MotorController::resume(){

}