#ifndef Maze_h
#define Maze_h

#include "Arduino.h"
#include "Cell.h"
#include "Position.h"

#define MAZE_X_SIZE 16
#define MAZE_Y_SIZE 16
#define MAX_X_POSITION 15
#define MAX_Y_POSITION 15
#define MIN_X_POSITION 0
#define MIN_Y_POSITION 0

class Maze {

public:
	Cell *cells[MAZE_X_SIZE][MAZE_Y_SIZE];
public:

	Maze();

	Cell* getCell(Position position);

	Cell* getYPlusCell(Position position);

	Cell* getXPlusCell(Position position);

	Cell* getYMinusCell(Position position);

	Cell* getXMinusCell(Position position);
};
#endif