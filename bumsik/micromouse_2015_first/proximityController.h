#ifndef ProximityController_h
#define ProximityController_h

#include "Arduino.h"

#include "sensor_infrad.h"

class ProximityController{
private:
	ProximitySensor centerSensor;
	ProximitySensor leftSensor;
	ProximitySensor rightSensor;
public:
	ProximityController(ProximitySensor left, ProximitySensor center, ProximitySensor right);
	void measureAll();
	char compareLeftAndRight();
	int getLeftProximity();
	int getRightProximity();
	int getFrontProximity();
};
#endif