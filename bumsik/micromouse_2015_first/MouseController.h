#ifndef MouseController_h
#define MouseController_h

#include "Arduino.h"
#include "Maze.h"
#include "Position.h"
#include "QueueArray.h"


class MouseController {

private:
	Maze *maze;
	Position currentPosition;
	Position nextPosition;
	QueueArray<Position> pathStack;

public:
	MouseController();

	void getDistanceAllCell();

	void initAllCell();

	void getShortestPath();

	void getNextCell();

	void getOrientation();

	char getHighestNeighbouringDistance(unsigned char x, unsigned char y);
};
#endif