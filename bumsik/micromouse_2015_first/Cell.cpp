#include "Cell.h"
#include "wall.h"
#include "WallDirection.h"

//direction of the cell is clockwise!!!

Cell::Cell() {
	Cell(unknown, unknown, unknown, unknown);
}

Cell::Cell(Wall y_plus_value, Wall x_plus_value, Wall y_minus_value, Wall x_minus_value) {
	this->walls[y_plus] = y_plus_value;
	this->walls[x_plus] = x_plus_value;
	this->walls[y_minus] = y_minus_value;
	this->walls[x_minus] = x_minus_value;
	this->distance = -1;
}

int Cell::getDistance() {
	this->distance;
}

void Cell::setDistance(int distance) {
	this->distance = distance;
}

Wall Cell::getWall(unsigned char direction) {
	return this->walls[direction];
}

void Cell::setWall(unsigned char direction, Wall value){
	this->walls[direction] = value;
}


