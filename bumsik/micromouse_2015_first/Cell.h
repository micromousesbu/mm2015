#ifndef Cell_h
#define Cell_h

#include "Arduino.h"
#include "wall.h"

class Cell {

private:
	Wall walls[4];	//The order is clockwiase, from y+ to x-
	int distance;	//distance from the current position

public:
	Cell();	//

	Cell(Wall y_plus_value, Wall x_plus_value, Wall y_minus_value, Wall x_minus_value);

	int getDistance();
	void setDistance(int distance);
	Wall getWall(unsigned char direction);
	void setWall(unsigned char direction, Wall value);
};
#endif