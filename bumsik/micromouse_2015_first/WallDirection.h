#ifndef WallDirection_h
#define WallDirection_h

#include "Arduino.h"


enum WallDirection {
	y_plus, x_plus, y_minus, x_minus
};
#endif