#ifndef MotorController_h
#define MotorController_h

#include "Arduino.h"

#include "motor_stepper.h"

class MotorController{

public:
	Motor leftMotor;
	Motor rightMotor;
	MotorController(Motor left, Motor right);
	void decreaseSteps(unsigned char step);
	void setSpeedRatings(unsigned char speed);
	void moveForward(int steps);
	void moveForward(int steps, unsigned char speedRating);
	void moveBackward(int steps);
	void moveBackward(int steps, unsigned char speedRating);
	void rotateLeft(float angle, float radius, unsigned char speedRating);
	void rotateRight(float angle, float radius, unsigned char speedRating);
	void pause();
	void resume();
	void stop();
};
#endif