#ifdef __cplusplus
extern "C" {
#endif

#include <Arduino.h>
// states in the FSM



typedef enum {
	in_center, going_front, turning_left, turning_right
} state;

// events in the FSM
// eol is a psuedo event used as a default in the state table
typedef enum {
	arrived, turned, empty_front, empty_left, empty_right, eol
} event;

// funtions to implement the task associated with a state transition
// all functions must have the same signature (parameters and return type)
// all functions are located in micromouse_2015_x.ino
extern void scan();
extern void goFrontCell();
extern void turnLeft();
extern void turnRight();

// global variable for present state of FSM
state present_state;

// declare type task_fn_ptr as a pointer to a task function
typedef void(*task_fn_ptr) ();

// A structure transition represents one row of a state transition table
// it has a field for the event, the next state, and a pointer to the
// task function.
// Declare type transition as a structure with fields for event value,
// next state value, and pointer to the task function

typedef struct {
	event event_val;
	state next_state;
	task_fn_ptr task_ptr;
} transition;

// The state transition table consistes of an array of arrays of structures.
// Each array of structures corresponds to a particular present state value.
// state for a given event value and the task function associated with the
// transition. Accordingly, each structure in an array has fields corresponding
// to an input value, the next state for this input value, and a pointer to
// the function task for this event value.
// The last transition structure in each array has a event value of eol.
// This is a default value meaning any key value that has not been explcity
// listed in a previous transition structure in the array.

const transition in_center_transitions[]= //subtalbe for in_center state
{
//	EVENT			NEXT_STATE		TASK			
	{empty_front,	going_front,	goFrontCell },
	{empty_left,	turning_left,	turnLeft},
	{empty_right,	turning_right,	turnRight },
	{eol,			in_center,		scan }
};

const transition going_front_transitions[]= //subtable for going_front state
{
//	EVENT		NEXT_STATE		TASK
	{arrived,	in_center,		scan },
	{ eol,		in_center,		scan }
};

const transition turning_left_transitons[] = //subtable for turning_left state
{
//	EVENT		NEXET_STATE		TASK
	{ turned,	in_center,		scan },
	{ eol,		in_center,		scan }
};

const transition turning_right_transitions[] = //subtable for turning_right state
{
	//EVENT		NEXET_STATE		TASK
	{ turned,	in_center,		scan },
	{ eol,		in_center,		scan }
};

// The outer array is an array of pointers to an array of transition
// structures for each present states.

void fsm(state* ps, event event_val);

#ifdef __cplusplus
}
#endif