#include "Arduino.h"
#include "CommonApps.h"

int CommonApps::filter_int(int min, int value, int max){
	if (value <= min)
		return min;
	else if (value >= max)
		return max;
	else
		return value;
}

bool CommonApps::is_in_range_int(int min, int value, int max){
	if (min <= value&&value <= max)
		return true;
	else
		return false;
}