#include "Arduino.h"
#include "motor_stepper.h"
#include "CommonApps.h"

Motor::Motor(){
}

Motor::Motor(unsigned char stepPin, unsigned char directionPin, bool isForwardClockwise){
	this->stepPin = stepPin;
	pinMode(stepPin, OUTPUT);
	this->directionPin = directionPin;
	pinMode(directionPin, OUTPUT);
	this->isForwardClockwise = isForwardClockwise;
	this->step = 0;
	this->speedRating = 255;
}

unsigned char Motor::getSpeedRating(){
	return this->speedRating;
}

void Motor::setSpeedRating(unsigned char speedRating){
	this->speedRating = speedRating;
}

int Motor::getStep(){
	return this->step;
}

void Motor::setStep(int step){
	this->step = step;
}

void Motor::decreaseStep(unsigned char step){
	this->step--;
	//this->step = CommonApps::filter_int(0, this->step - step, Motor::MAX_STEP);
}

void Motor::rotateForward(int step){
	this->step = step;
	switch (this->isForwardClockwise){
		//go clockwise
	case true: 
		digitalWrite(this->directionPin, HIGH);
		break;
		//go counterclockwise
	case false :
		digitalWrite(this->directionPin, LOW);
		break;
	}
}

void Motor::rotateBackward(int step){
	switch (this->isForwardClockwise){
		//go counterclockwise
	case true:
		digitalWrite(this->directionPin, LOW);
		this->step = step;
		break;
		//go clockwise
	case false:
		digitalWrite(this->directionPin, HIGH);
		this->step = step;
		break;
	}
}