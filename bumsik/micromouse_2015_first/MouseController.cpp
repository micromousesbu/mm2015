#include "MouseController.h"
#include "Maze.h"
#include "Position.h"
#include "StackArray.h"

#define X_DESTINATION 6
#define Y_DESTINATION 6


MouseController::MouseController(){
	maze = new Maze();
}

void MouseController::initAllCell(){
	unsigned char x;
	unsigned char y;
	//Fill the last
	for (x = MIN_X_POSITION; x <= MAX_X_POSITION; x++){
		for (y = MIN_Y_POSITION; y <= MAX_Y_POSITION; y++){
			this->maze->cells[x][y]->setDistance(-1);
		}
	}
}

void MouseController::getDistanceAllCell(){
	unsigned char currentPathDistance = 1; // This is how far the 'water' has flowed
	unsigned char x;
	unsigned char y;
	this->initAllCell();
	this->maze->cells[currentPosition.x][currentPosition.y]->setDistance(0);
	while (1){
		for (x = MIN_X_POSITION; x <= MAX_X_POSITION; x++) { //Creating a loop which scans the whole maze
			for (y = MIN_Y_POSITION ; y <= MAX_Y_POSITION; y++) {
				if (this->maze->cells[x][y]->getDistance() != -1) //If the cell has already been reached, then continue to the next cell
					continue;
				if (getHighestNeighbouringDistance(x, y) != -1) //If there is a neighbouring cell which has been
					this->maze->cells[x][y]->setDistance(currentPathDistance); //reached, then you have reached the current cell
				//so give it a value
			}
		}
		if (maze->cells[X_DESTINATION][Y_DESTINATION]->getDistance() != -1) //If the destination cell has a value after a sweep, the algorithm ends
			break;
		currentPathDistance++; //Increment the distance because we are scanning again.
	}
	//The highestNeighbouringCell(x,y) function returns the value of the highest, accessible (ie there
	//are no separating walls) neighbouring cell.

	//Initially, all the cells are given a value of -1, except the micromouse's current cell, which is given value 0.
	//Then the grid of cell values is scanned. On the first scan, only the cells next to, and accessible to the cell
	//where the micromouse is, will be given a (non '-1') value. That value is 1. This is repeated untill the
	//destination cell has been given a (non '-1') value.
}

char MouseController::getHighestNeighbouringDistance(unsigned char x, unsigned char y) {
	//peek Y plus side
	if (maze->cells[x][y]->getWall(0) != exist && y < MAX_Y_POSITION)
		return maze->cells[x][y + 1]->getDistance();
	else if (maze->cells[x][y]->getWall(1) != exist && x < MAX_X_POSITION)
		return maze->cells[x + 1][y]->getDistance();
	else if (maze->cells[x][y]->getWall(2) != exist && y > MIN_Y_POSITION)
		return maze->cells[x][y - 1]->getDistance();
	else 
		return maze->cells[x - 1][y]->getDistance();
}


void MouseController::getShortestPath(){
	StackArray<Position> availablePositionStack = StackArray<Position>();
	StackArray<Position> pathStack = StackArray<Position>();
	Position position(currentPosition.x, currentPosition.y);
	int currentDistance = 0;
	//set first stack
	availablePositionStack.push(Position(position.x, position.y));
	bool isFound = false;

	while (1){
		//copy the next position
		position = availablePositionStack.peek();
		currentDistance = maze->cells[position.x][position.y]->getDistance();
		//if currentposition is goal, break
		if (position.x == X_DESTINATION && position.y == Y_DESTINATION){
			isFound = true;
			pathStack.push(availablePositionStack.pop());
			break;
		}
		// Look around in counter-clockwise
		if ((maze->cells[position.x - 1][position.y]->getDistance() == (currentDistance + 1)) && position.x > MIN_X_POSITION){
			isFound = true;
			pathStack.push(availablePositionStack.pop());
			availablePositionStack.push(Position(position.x - 1, position.y));
		}

		if ((maze->cells[position.x][position.y - 1]->getDistance() == (currentDistance + 1)) && position.y > MIN_Y_POSITION){
			if (!isFound){
				pathStack.push(availablePositionStack.pop());
				isFound = true;
			}
			availablePositionStack.push(Position(position.x, position.y - 1));
		}
		if ((maze->cells[position.x + 1][position.y]->getDistance() == (currentDistance + 1)) && position.x < MAX_X_POSITION){
			if (!isFound){
				pathStack.push(availablePositionStack.pop());
				isFound = true;
			}
			availablePositionStack.push(Position(position.x + 1, position.y));
		}

		if ((maze->cells[position.x][position.y + 1]->getDistance() == (currentDistance + 1)) && position.y < MAX_Y_POSITION){
			if (!isFound){
				pathStack.push(availablePositionStack.pop());
				isFound = true;
			}
			availablePositionStack.push(Position(position.x, position.y + 1));
		}

		if (!isFound){ //if no available next cell
			availablePositionStack.pop();
			//pop pathstack until it meet next availableStack
			while (!(
				(pathStack.peek().x == availablePositionStack.peek().x) && (pathStack.peek().y + 1 == availablePositionStack.peek().y) && pathStack.peek().y < MAX_Y_POSITION ||
				(pathStack.peek().x + 1 == availablePositionStack.peek().x) && (pathStack.peek().y == availablePositionStack.peek().y) && pathStack.peek().x > MAX_X_POSITION ||
				(pathStack.peek().x == availablePositionStack.peek().x) && (pathStack.peek().y - 1 == availablePositionStack.peek().y) && pathStack.peek().y > MIN_Y_POSITION ||
				(pathStack.peek().x - 1 == availablePositionStack.peek().x) && (pathStack.peek().y == availablePositionStack.peek().y) && pathStack.peek().x > MIN_X_POSITION
				))
				pathStack.pop();
		}
	}
	//reverse-copy of the pathStack
	int i;
	for (i = 0; i < pathStack.count(); i++)
		this->pathStack.push(pathStack.pop());
}

void MouseController::getNextCell(){
}

void MouseController::getOrientation(){
}


