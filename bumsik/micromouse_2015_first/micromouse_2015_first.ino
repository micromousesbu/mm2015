#include "motor_stepper.h"
#include "motorController.h"
#include "sensor_infrad.h"
#include "proximityController.h"
#include "fsm_table.h"
#include "MouseController.h"

//Set motors - pin 6,7,11,12 used
//timer/counter 0 and 2 used
Motor leftMotor(9, 8, true);
Motor rightMotor(5, 6, false);
MotorController motorController(leftMotor, rightMotor);

//Set Sensors - Pin 0,1,2 used
//Note that the left and right sensor are crossed(reversed)
ProximitySensor leftSensor(0);
ProximitySensor centerSensor(1);
ProximitySensor rightSensor(2);
ProximityController proximityController(leftSensor, centerSensor, rightSensor);

/* ================================For arduino Uno!=========================
//Set motors - pin 6,7,11,12 used
//timer/counter 0 and 2 used
Motor leftMotor(6,  7 , true);
Motor rightMotor(11, 12, false);
MotorController motorController(leftMotor, rightMotor);

//Set Sensors - Pin 0,1,2 used
//Note that the left and right sensor are crossed(reversed)
ProximitySensor leftSensor(0);
ProximitySensor centerSensor(1);
ProximitySensor rightSensor(2);
ProximityController proximityController(leftSensor, centerSensor, rightSensor);
*/

//Set the MouseController
MouseController *mousecontroller = new MouseController();

void setup() {
  // put your setup code here, to run once:

	//reference is internal 5V
	analogReference(DEFAULT); 


	// Set timer for the Proximity Sensors
	// timer/counter 4 used -10 bit counter
	// No CTC Mode for T/C4 so we are using Normal mode and Set TCNT4 when OC4A interrupt
	TCCR4A = 0;
	// 7:6 Pin = COM4A1:0, set 00. 
	TCCR4B = 0x00 | 0x0D;		// clock scale = clk/4096
	// for Nomal mode, SET PWM4X(7) = 0
	// Bits 3:0 = CS43:CS40 the clock is devided by 2^(3:0).
	// When the normal mode, the TOP is OCR4C and when TOP, TOV4 Flage set on
	// But OCR4C is - 8bit
	OCR4C = 255;		// 16MHz/(1024*256*3)=20.35Hz
	TIMSK4 = 0x02;		
	// Bit2 - TOIE4 interrupt Enable


	// Set timer for the leftMotor
	//timer/counter 1 used
	// CTC Mode and OC1A interrupt
	// Output Compara Match A Pin is Pin 9
	TCCR1A = 0x40;
	//COM1A1:0 is Bit7:6, Set 01 to toggle, Set 0 not to toggle. WGM11:WGM10 - set 00
	TCCR1B = 0x08|0x05;						// clock scale = clk/N, not Clk/1024
	// WGM13:12 is Bit 4:3, set 01 for CTC Mode. CS12:CS10 is Pin 2:0. set 5 for N =1024, 4 for N = 256, 3 for N=64
	OCR1A = motorController.leftMotor.getSpeedRating();	//Maximum speed is 7,813Hz and minimum is 30.64Hz
	TIMSK1 = 0x02;
	//OCIE1A = 1 : Interrupt enable

	// Set timer for the rightMotor
	//timer/counter 3 used 
	// CTC Mode and OC1A interrupt
	// Output Compara Match A Pin is Pin 5
	TCCR3A = 0x40;
	//COM1A1:0 is Bit7:6, Set 01 to toggle, Set 0 not to toggle. WGM11:WGM10 - set 00
	TCCR3B = 0x08 | 0x05;						// clock scale = clk/N, not Clk/1024
	// WGM13:12 is Bit 4:3, set 01 for CTC Mode. CS12:CS10 is Pin 2:0. set 5 for N =1024, 4 for N = 256, 3 for N=64
	OCR3A = motorController.rightMotor.getSpeedRating();	//Maximum speed is 7,813Hz and minimum is 30.64Hz
	TIMSK3 = 0x02;
	//OCIE1A = 1 : Interrupt enable
	/* ================================For arduino Uno!=========================

	//Set timer for the Proximity Sensors
	//timer/counter 1 used
	TCCR1A = 0;
	TCCR1B = 0x08|0x05;		// clock scale = clk/1024
	TCCR1C = 0;
	OCR1A = 768;		// 16MHz/(1024*256*3)=20.35Hz
	TIMSK1 = 0x02;		// OCIE1A interrupt enable

	// Set timer for the leftMotor
	//timer/counter 0 used - wait()statement disabled!
	TCCR0A = 0x42;
	TCCR0B = 0x05;						// clock scale = clk/1024
	OCR0A = motorController.leftMotor.getSpeedRating();	//Maximum speed is 7,813Hz and minimum is 30.64Hz
	TIMSK0 = 0x02;

	// Set timer for the rightMotor
	//timer/counter 2 used 
	TCCR2A = 0x42;
	TCCR2B = 0x07;						// clock scale = clk/1024
	OCR2A = motorController.rightMotor.getSpeedRating();	//Maximum speed is 7,813Hz and minimum is 30.64Hz
	TIMSK2 = 0x02;
	*/

	interrupts();		// Enable interrupt
	motorController.moveForward(1000, 10);
	//====================================================
	//					SPEED TABLE
	//	TCCR0B	OCR0A	FEQ		TCCR2B	OCR2A	FEQ
	//	0x05	255		30.53	0x07	255		30.49
	//			100		77.2			100		77.2
	//			50		153.8			50		152.7
	//			40		190				40		190
	//			30		250				30		253
	//			10		710				10		690    	//looks like a proper speed
	//			5		1.33kHz			5		1.33kHz
	//====================================================
}

void loop() {
  // put your main code here, to run repeatedly:

	present_state = in_center;
	fsm(&present_state, eol);
	while (1){};
	
}



// Timer 4 interrupt
// It measures proximity about 20 times per second
ISR(TIMER4_COMPA_vect){
	proximityController.measureAll();
	//Auto Correction
	switch (proximityController.compareLeftAndRight())
	{
	case 1 :	//when left side is closer
		//turn right
		break;
	case 0 :	//both are OK
		//Nothing?
		break;
	case -1 :	//when right side is closer
		//turn left
		break;
	default:
		break;
	}
};

// Timer 1 interrupt
// for the  left motor
ISR(TIMER1_COMPA_vect){


	//Serial.print("left");
	//Serial.println(motorController.leftMotor.getStep());
	OCR1A = motorController.leftMotor.getSpeedRating();

	if (motorController.leftMotor.getStep() == 0){
		TCCR1A = 0x00;	//Stop toggleing!
		return;
	}
	motorController.leftMotor.decreaseStep(1);
	return;
};

// Timer 2 interrupt
// for the right motor
ISR(TIMER3_COMPA_vect){

	//Serial.print("right");
	//Serial.println(motorController.rightMotor.getStep());
	OCR3A = motorController.rightMotor.getSpeedRating();
	if (motorController.rightMotor.getStep() == 0){
		TCCR3A = 0x00;	//Stop toggleing!
		return;
	}
	motorController.rightMotor.decreaseStep(1);
};
void scan(){
	mousecontroller->initAllCell();
	mousecontroller->getDistanceAllCell();
	/*
	void scan(){
	

	
	getTheShortestPath();
	getNextCell();
	getOrientation();
	moveForward();
	*/
};

void goFrontCell(){

};

void turnLeft(){

};

void turnRight(){

};