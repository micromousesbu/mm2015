int rdirpin = 12;
int rsteppin = 13;
int ldirpin = 10;
int lsteppin = 11;

//reprensent the each cell
typedef struct{
  byte distance;
  byte walls;
} cell;

cell maze[16][16];

//current position
int row = 0;
int col = 0;

void setup() {
  // put your setup code here, to run once:
  //initialize the array
  maze[7][8].distance = 0;
  maze[8][8].distance = 0;
  maze[8][7].distance = 0;
  maze[8][8].distance = 0;

  //setup motor control pin
  pinMode(rdirpin, OUTPUT);
  pinMode(rsteppin, OUTPUT);
  pinMode(ldirpin, OUTPUT);
  pinMode(lsteppin, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  
  //check if in centerrun mode
  
  //check if back to start position
  
  //detect walls
  
  //visited or not
  
  //update the maze
  updateMap(r,c,w);
  
  //motor test
  move(0,-90);
  delay(1000);
  move(0,90);
  delay(1000);
  move(1,0);
  exit(0);
}

//move function can be used during mapping stage
//cells used for moving how many cells, and degree used
//for turn which way. left/right/180
void move(int cells, int degree){
  int i;
  //autocorrect();
  switch(degree){
    case 90:  //right turn 90 degree
      digitalWrite(rdirpin,HIGH);
      digitalWrite(ldirpin,LOW);
      for(i=0; i<578; i++){
        digitalWrite(rsteppin,LOW);
        digitalWrite(lsteppin,LOW);
        digitalWrite(rsteppin,HIGH);
        digitalWrite(lsteppin,HIGH);
        delayMicroseconds(500);
      }
      break;
    case -90:  //left turn 90 degree
      digitalWrite(rdirpin,LOW);
      digitalWrite(ldirpin,HIGH);
      for(i=0; i<578; i++){
        digitalWrite(rsteppin,LOW);
        digitalWrite(lsteppin,LOW);
        digitalWrite(rsteppin,HIGH);
        digitalWrite(lsteppin,HIGH);
        delayMicroseconds(500);
      }
      break;
    case 180:  //turn around
      digitalWrite(rdirpin,HIGH);
      digitalWrite(ldirpin,LOW);
      for(i=0; i<1156; i++){
        digitalWrite(rsteppin,LOW);
        digitalWrite(lsteppin,LOW);
        digitalWrite(rsteppin,HIGH);
        digitalWrite(lsteppin,HIGH);
        delayMicroseconds(500);
      }
      break;
    default:
      break;
  }
  digitalWrite(rdirpin,LOW);
  digitalWrite(ldirpin,LOW);
  
  int steps=1540*cells;

  for(i=0; i<steps;i++){
    /*if((i%200)==0){
       int off=autocorrect();
       if(off==right){
         //robot close to right. add more stepps to right motor
       }
       else if(off==left){
         //robot close to left, add more stepps to left motor
       }
    }*/
    digitalWrite(rsteppin,LOW);
    digitalWrite(lsteppin,LOW);
    digitalWrite(rsteppin,HIGH);
    digitalWrite(lsteppin,HIGH);
    delayMicroseconds(500);
  } 
}

void updateMap(int r, int c, int w){
  byte tmp=B0;
  //generate the byte representation of the walls
  maze[r][c].walls=tmp;
  //generate the new distance
  byte olddis = 
  
  
  
  maze[r][c].distance = newdis;
  //update old distance
  
}


